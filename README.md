# Reconfigure Stylesheet

Add and delete stylesheets in a Mendix application.

The path for the stylesheet to be added can be hardcoded or created based on attributes of the context object.
To declare the set of stylesheets to be removed a regular expression can be used. If an existing stylesheet matches the expression it will be removed.

This combination gives the possibility to dynamically load and unload css stylesheets.